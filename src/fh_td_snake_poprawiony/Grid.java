package fh_td_snake_poprawiony;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.Color;
import javax.swing.JLabel;

public class Grid {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Grid window = new Grid();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Grid() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		Game snakeGame = new Game();
		frame.setBounds(100, 100, 1000, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JSlider slider = new JSlider();
		slider.setValue(1);
		slider.setMaximum(10);
		slider.setMinimum(1);
		slider.setMajorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setBounds(420, 620, 100, 20);
		slider.setFocusable(false);
		
		frame.getContentPane().add(snakeGame);
		
		JLabel lblSnakeSpeed = new JLabel("Snake speed (0-10)");
		
		JLabel lblNacinijStrzakAby = new JLabel("Naci\u015Bnij strza\u0142k\u0119 aby zacz\u0105\u0107 gr\u0119");
		
		JLabel lblNacinijSpacjAby = new JLabel("Naci\u015Bnij spacj\u0119 aby zrestartowa\u0107 gr\u0119");
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(53)
							.addComponent(lblNacinijStrzakAby)
							.addGap(48)
							.addComponent(lblNacinijSpacjAby)
							.addGap(73)
							.addComponent(lblSnakeSpeed)
							.addGap(26)
							.addComponent(slider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(snakeGame, GroupLayout.DEFAULT_SIZE, 964, Short.MAX_VALUE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(snakeGame, GroupLayout.DEFAULT_SIZE, 609, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(slider, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblNacinijStrzakAby, GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
							.addComponent(lblNacinijSpacjAby)
							.addComponent(lblSnakeSpeed)))
					.addContainerGap())
		);
		frame.getContentPane().setLayout(groupLayout);
		frame.setVisible(true);
	    
		
		slider.addChangeListener(new ChangeListener() {
	        public void stateChanged(ChangeEvent e) {
	        snakeGame.setTimer(slider.getValue());
	        }
	      });
		snakeGame.setTimer(slider.getValue());
		
		
//		btnStart.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//			snakeGame.startGame();
//			snakeGame.setTimer(slider.getValue());
//
//
//			}
//		});
		
	}
}
