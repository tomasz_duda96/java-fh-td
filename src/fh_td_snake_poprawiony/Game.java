package fh_td_snake_poprawiony;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Game extends JPanel implements ActionListener, ChangeListener, Runnable{

	private int[] snakexlength = new int [1000];
	private int[] snakeylength = new int [1000];
	
	private boolean left = false;
	private boolean right = false;
	private boolean up = false;
	private boolean down = false;
	
	private int snakelength = 3;
	private int moves = 0;

	private int [] delay = {1000, 900, 800, 700, 600, 500, 400, 300, 200, 100};
	private Timer timer = new Timer(delay[0], this);
	private Listener listener = new Listener();
	private Thread thread;

	
	private int [] applecellx = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350,
								375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700,
								725, 750, 775, 800, 825, 850};
	
	private int [] applecelly = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350,
								375, 400, 425, 450, 475, 500, 525, 550, 575};
	
	
	private Random random = new Random();
	private int xpos = random.nextInt(34);
	private int ypos = random.nextInt(23);
	
	
	
	
	public Game() 
	{		
		this.startGame();
		timer.start();
		addKeyListener(listener);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);	
	}
	 
	
	public void setTimer(int x)
	{
		this.timer.stop();
		this.timer = new Timer(delay[x-1], this);
		this.timer.start();
	}
	
	public void startGame()
	{
		this.thread = new Thread(this);
		this.thread.setDaemon(true);
		this.thread.start();
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		/* Pocz�tkowa pozycja snake'a */
		if(moves == 0)
		{
			snakexlength[2] = 50;
			snakexlength[1] = 75;
			snakexlength[0] = 100;
			
			snakeylength[2] = 100;
			snakeylength[1] = 100;
			snakeylength[0] = 100;
		}
		
		/* Rysowanie planszy */
		g.drawRect(24, 24, 851, 577);
		g.setColor(Color.BLACK);
		g.fillRect(25, 25, 850, 575);
		
		
		/* Rysowanie weza */
		g.setColor(Color.GREEN);	
		for(int x = 0; x < snakelength; x++)
		{
			if(x == 0)
			{
				g.drawRect(snakexlength[x], snakeylength[x], 10, 10);
				g.fillRect(snakexlength[x], snakeylength[x], 10, 10);
			}
			else
			{
				g.drawRect(snakexlength[x], snakeylength[x], 10, 10);
			}
		}
		
		/* Zjedzenie jab�ka */
		if(applecellx[xpos] == snakexlength[0] && applecelly[ypos] == snakeylength[0])
		{
			snakelength++;
			xpos = random.nextInt(34);
			ypos = random.nextInt(23);	
		}
		
		/* Kolizja glowy w�a */
		for(int x = 1 ; x < snakelength ; x++)
		{
			if(snakexlength[x] == snakexlength[0] && snakeylength[x] == snakeylength[0])
			{
				left = false;
				right = false;
				up = false;
				down = false;
				
				g.setColor(Color.WHITE);
				g.setFont(new Font("arial", Font.BOLD, 50));
				g.drawString("GAME OVER", 300, 300);
				
				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Naci�nij spacj� aby spr�bowa� ponownie", 270, 340);
				stop(); 
			}
		}
				
		/* Rysowanie jab�ka */
		g.setColor(Color.RED);
		g.drawRect(applecellx[xpos], applecelly[ypos], 10, 10);
		
		
		g.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

	/* Od�wie�anie planszy */
			if(right)
			{
				for(int x = snakelength-1 ; x >= 0 ; x--)
				{
					snakeylength[x+1] = snakeylength[x];
				}
				for(int x = snakelength-1; x >= 0 ; x--)
				{
					if(x == 0)
					{
						snakexlength[x] = snakexlength[x] + 25;
					}
					else
					{
						snakexlength[x] = snakexlength[x-1];
					}
					if(snakexlength[x] > 850)
					{
						snakexlength[x] = 25;
					}
				}
				
				repaint();
			}
			if(left)
			{
				for(int x = snakelength-1 ; x >= 0 ; x--)
				{
					snakeylength[x+1] = snakeylength[x];
				}
				for(int x = snakelength-1; x >= 0 ; x--)
				{
					if(x == 0)
					{
						snakexlength[x] = snakexlength[x] - 25;
					}
					else
					{
						snakexlength[x] = snakexlength[x-1];
					}
					if(snakexlength[x] < 25)
					{
						snakexlength[x] = 850;
					}
				}
				
				repaint();	
			}
			if(down)
			{
				for(int x = snakelength-1 ; x >= 0 ; x--)
				{
					snakexlength[x+1] = snakexlength[x];
				}
				for(int x = snakelength-1; x >= 0 ; x--)
				{
					if(x == 0)
					{
						snakeylength[x] = snakeylength[x] + 25;
					}
					else
					{
						snakeylength[x] = snakeylength[x-1];
					}
					if(snakeylength[x] > 575)
					{
						snakeylength[x] = 25;
					}
				}
				
				repaint();
			}
			if(up)
			{
				for(int x = snakelength-1 ; x >= 0 ; x--)
				{
					snakexlength[x+1] = snakexlength[x];
				}
				for(int x = snakelength-1; x >= 0 ; x--)
				{
					if(x == 0)
					{
						snakeylength[x] = snakeylength[x] - 25;
					}
					else
					{
						snakeylength[x] = snakeylength[x-1];
					}
					if(snakeylength[x] < 25)
					{
						snakeylength[x] = 575;
					}
				}
				
				repaint();
			}
	}

	
	private class Listener implements KeyListener{ 
		
		/* Nas�uchiwanie zmiany kierunku */
		@Override
		public void keyPressed(KeyEvent e) {
		
			if(e.getKeyCode() == KeyEvent.VK_SPACE)
			{
				moves = 0;
				snakelength = 3;
				repaint();
			}
			
			if(e.getKeyCode() == KeyEvent.VK_RIGHT)
			{
				moves++;
				right = true;
				
				if(!left)
				{
					right = true;
					
				}
				else
				{
					right = false;
					left = true;
				}
				
				up = false;
				down = false;
					
			}
			
			if(e.getKeyCode() == KeyEvent.VK_LEFT && moves != 0)
			{
				moves++;
				left = true;
				
				if(!right)
				{
					left = true;
					
				}
				else
				{
					left = false;
					right = true;
				}
				
				up = false;
				down = false;
				
			}
			
			if(e.getKeyCode() == KeyEvent.VK_UP)
			{
				moves++;
				up = true;
				
				if(!down)
				{
					up = true;
					
				}
				else
				{
					up = false;
					down = true;
				}
				
				left = false;
				right = false;
					
			}
			
			if(e.getKeyCode() == KeyEvent.VK_DOWN)
			{
				moves++;
				down = true;
				
				if(!up)
				{
					down = true;
					
				}
				else
				{
					down = false;
					up = true;
				}
				
				left = false;
				right = false;
					
			}
		}
		
		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
	}
	




	@Override
	public void stateChanged(ChangeEvent e) {

		
	}


	@Override
	public void run() {
		timer.start();
		
	}
	

	public void stop() {
				
		repaint();
		
		try {
			this.thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
}
